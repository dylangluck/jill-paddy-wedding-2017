//
// RSVP AJAX
//

var $rsvpForm = $('#rsvp-form');
var $rsvpSubmit = $rsvpForm.find('button[type=submit]');

$rsvpForm.on('submit', function(e){

  e.preventDefault();

  var $inputs = $rsvpForm.find('input');
  var hasError = false;

  $.each($inputs, function(i, input) {
    if(!$(input).val()) {
      $(input).parent().addClass('error');
      hasError = true;
    }
  });

  if(hasError == true) {
    return false;
  }

  var $action = $rsvpForm.attr('action');
  var $method = $rsvpForm.attr('method');

  $.ajax({
    url: $action,
    method: $method,
    data: $rsvpForm.serialize(),
    success: function(){
      $rsvpForm.fadeOut();
      $('.rsvp-success').fadeIn();
    }
  });

});
