//
// Global Variables
//

// Elements
var $window = $(window);
var $document = $(document);
var $body = $('body');

//
// Device Info
//

// Is Touch Device ?
var isMobile = Modernizr.touch;

//
// Init Slider
//

function initSlider(){
  $('.jp-slider').slick({
    adaptiveHeight: true,
    autoplay: true,
    arrows: false,
    draggable: false,
    fade: true,
    speed: 1000,
    pauseOnFocus: false,
    pauseOnHover: false,
    swipe: false,
    touchMove: false
  });
}


//
// Page Load Animation
//


// Only fire animation first load
window.onload = function () {

  $('.jp-loader').hide();

  if (localStorage.getItem("navigation_item_clicked") === null || localStorage.getItem("navigation_item_clicked") === 'false') {

    // Elements
    var $wrap = $('.jp-wrap');
    var $contentItems = $('.jp-container').children();

    // Timeline
    var pageTL = new TimelineMax({ paused:true });

    // Set Before State
    TweenLite.set($contentItems, {y: 10, opacity: 0});
    TweenLite.set($wrap, {width: '0%'});

    // Define Animation
    pageTL
      .to($wrap, 1, {width: '95%', ease: 'ease-in'}, '+=.2')
      .add(function(){
        initSlider();
      })
      .staggerTo($contentItems, 1, {y: 0, opacity: 1, ease: 'ease-in'}, 0.2, '-=.6');

    pageTL.play(0);

  } else {
    localStorage.setItem("navigation_item_clicked", 'false');
    initSlider();
  }
}

// Set LocalStorage Flag
$('.jp-navigation__item').on('click', function(){
  localStorage.setItem("navigation_item_clicked", 'true');
});
